<!-- Begin Top -->
	<?php if ( ! is_page( array( 'formato-de-contrato-de-arrendamiento', 'arriendos-inmuebles', 'ventas-inmuebles' ) ) ) : ?>
	<section class="top_1" data-wow-delay="0.5s">
		<div class="row align-center align-middle">
			<div class="small-12 columns">
				<?php dynamic_sidebar( 'data' ); ?>
			</div>
		</div>
	</section>
	<?php endif; ?>
	<section class="top" data-wow-delay="0.5s">
		<div class="row align-center align-middle">
			<div class="small-12 medium-3 columns">
				<?php dynamic_sidebar( 'logo' ); ?>
			</div>
			<div class="small-12 medium-9 columns">
				<?php if ( ! is_page( array( 'formato-de-contrato-de-arrendamiento', 'arriendos-inmuebles', 'ventas-inmuebles' ) ) ) : ?>
				<?php get_template_part( 'part', 'menu' ); ?>
				<?php else : ?>
				<?php dynamic_sidebar( 'social_media_landing' ); ?>
				<?php endif; ?>
			</div>
		</div>
	</section>
<!-- End Top -->