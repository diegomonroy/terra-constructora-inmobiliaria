<!-- Begin Banner -->
	<section class="banner" data-wow-delay="0.5s">
		<div class="row collapse expanded">
			<div class="small-12 columns">
				<?php if ( is_front_page() ) : dynamic_sidebar( 'banner_inicio' ); endif; ?>
				<?php if ( is_page( array( 'quienes-somos' ) ) ) : dynamic_sidebar( 'banner_quienes_somos' ); endif; ?>
				<?php if ( is_page( array( 'arriendos-1' ) ) ) : dynamic_sidebar( 'banner_arriendos' ); endif; ?>
				<?php if ( is_page( array( 'ventas-1' ) ) ) : dynamic_sidebar( 'banner_ventas' ); endif; ?>
				<?php if ( is_page( array( 'avaluos-1' ) ) ) : dynamic_sidebar( 'banner_avaluos' ); endif; ?>
				<?php if ( is_page( array( 'documentacion' ) ) ) : dynamic_sidebar( 'banner_documentacion' ); endif; ?>
				<?php if ( is_page( array( 'contactenos' ) ) ) : dynamic_sidebar( 'banner_contactenos' ); endif; ?>
				<?php if ( is_page( array( 'formato-de-contrato-de-arrendamiento' ) ) ) : dynamic_sidebar( 'banner_formato_de_contrato_de_arrendamiento' ); endif; ?>
				<?php if ( is_page( array( 'arriendos-inmuebles' ) ) ) : dynamic_sidebar( 'banner_arriendos_inmuebles' ); endif; ?>
				<?php if ( is_page( array( 'ventas-inmuebles' ) ) ) : dynamic_sidebar( 'banner_ventas_inmuebles' ); endif; ?>
			</div>
		</div>
	</section>
<!-- End Banner -->