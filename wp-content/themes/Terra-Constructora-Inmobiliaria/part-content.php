<?php
$style = '';
if ( is_page( array( 'inmuebles' ) ) ) :
	$style = 'inside';
endif;
?>
<div class="<?php echo $style; ?>">
	<?php if ( is_front_page() || is_page( array( 'inmuebles' ) ) ) : get_template_part( 'part', 'search' ); endif; ?>
</div>
<?php get_template_part( 'part', 'banner' ); ?>
<!-- Begin Content -->
	<section class="content" data-wow-delay="0.5s">
		<div class="row">
			<div class="small-12 columns">
				<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
					<?php the_content(); ?>
				<?php endwhile; endif; ?>
			</div>
		</div>
	</section>
<!-- End Content -->